filter_packages = [
    ("^(BaseOS|AppStream|HighAvailability|NFV|RT|ResilientStorage)$", {
        "*": [
            "centos-linux-repos", # We want to force-exclude centos-linux repos from the resolver otherwise this lands in BaseOS and centos-stream-repos lands in AppStream
            "python36",  # RCM-42305 - ursine python36 should be only in buildroot
            "gcc-toolset-9-*-testsuite", #RCM-58061
            "gcc-toolset-9-gcc-plugin-devel", #RCM-58061
            "tuned-profiles-sap",       # RCM-53604 - This should be only in SAP
            "tuned-profiles-sap-hana",  # RCM-53604 - This should be only in SAPHANA
            "java-*slowdebug*",              # Jira: RHELCMP-1407 Jira: RHELCMP-1512 Jira: RHELCMP-4189
            "java-*fastdebug*",              # Jira: RHELCMP-1407 Jira: RHELCMP-1512 Jira: RHELCMP-4189
        ]
    }),

    ("^BaseOS$", {
        "*": [
            "compat-openssl10-devel",  # RCM-26416
            "compat-openssl10-pkcs11-helper",  # RCM-28614
            "openldap-servers-debuginfo",  # RCM-28225
            "ongres-scram",  # RCM-31750
            "libcomps-devel", #RhBug 1960616
        ]
    }),

    ("^RT$", {
        "*": [
            "kernel-rt-kvm-debuginfo",  # RCM-33741
            "kernel-rt-debug-kvm-debuginfo",  # RCM-33741
        ]
    }),

    ("^.*$", {
        "*": [
            "kernel-*-internal",  # RCM-54000
            "kpatch-patch-*-internal", # RHELCMP-991
            "scap-security-guide-rule-playbooks",
        ]
    }),
]

additional_packages = [
    ("^AppStream$", {
        "*": [
	    "ansible-collection-redhat-rhel_mgmt", #RHELCMP-6300 RhBug 1992497
            "libasan5", #RCM-61474
            "libubsan1",
            "dotnet5.0", # Jira: RHELCMP-1966
            "gcc-toolset-10-dyninst-devel", # Jira: RHELCMP-2173
            "gnome-software-editor",
            "pipewire-docs",
            "rt-tests", #RhBug 1877587
            "micropipenv", #RHELCMP-2297 RhBug 1876890
            "nispor", #RHELCMP-2300 RhBug 1876979
            "nispor-devel", #RHELCMP-2300 RhBug 1876979
            "python3-nispor", #RHELCMP-2300 RhBug 1876979
            "python3-pyghmi", #RHELCMP-6299 RhBug 1992470
            "libnumbertext", #RHELCMP-2856 RhBug 1888652
            "dotnet-sdk-3.0",
            "perl-IO-String", #RHELCMP-3042
            "stalld", #RHELCMP-2941, RhBug 1893374
            "dejavu-lgc-sans-fonts", #RhBug 1857213
            "xorg-x11-drivers", #RHELCMP-3014
            "fstrm", # Jira: RHELCMP-3011
            "fstrm-devel", # Jira: RHELCMP-3752
            "ucx-cma",
            "ucx-ib",
            "ucx-rdmacm"
            "ucx-devel", #RhBug 1851724
            "mpich-doc", #RhBug 1732982
            "mvapich2-devel",
            "mvapich2-psm2-devel",
            "mvapich2-doc", #RhBug 1850084
            "libecpg", #RHBug 1903638, RHELCMP-3431
            "rsyslog-udpspoof",
            "mysql-selinux", #RhBug 1909064, RHELCMP-3600
            "rshim", #RhBug 1915862, RHELCMP-3793
            "qatlib", #RhBug 1919586, RHELCMP-3871
            "qatengine", # RHELCMP-5642
            "tracer", #RhBug 1919665, RHELCMP-3872
            "flatpak-xdg-utils", #RhBug 1925468, RHELCMP-4083
            "modulemd-tools", #RhBug 1927050, RHELCMP-4123
            "emoji-picker", # RHELCMP-4254
            "gcc-toolset-11", # RHBug 1960302, RHELCMP-5395
            "xapian-core", # RHBug 1960197, RHELCMP-5391
            "gnome-session-kiosk-session", # Jira: RHELCMP-5372
            "udftools", # RHBug 1963090, RHELCMP-5460
            "qperf", #RhBug 1933283
            "gcc-toolset-11-dwz", #RHBug 1965353, RHELCMP-5534
            "gcc-toolset-11-gcc", #RHBug 1965355, RHELCMP-5535
            "ansible-freeipa-tests", #RhBug 1967109
            "xorg-x11-server-Xwayland", #RHBug 1966560, RHELCMP-5560
            "gcc-toolset-11-strace", #RHBug 1967364, RHELCMP-5570
            "ansible-pcp", #RHBug 1967392, RHELCMP-5571
            "gcc-toolset-11-dyninst", #RHBug 1967749, RHELCMP-5584
            "gcc-toolset-11-binutils", #RHBug 1967652, RHELCMP-5578
            "gcc-toolset-11-valgrind", #RHBug 1967755, RHELCMP-5586
            "gcc-toolset-11-elfutils", #RHBug 1967753, RHELCMP-5585
            "gcc-toolset-11-systemtap", #RHBug 1967759, RHELCMP-5587
            "gcc-toolset-11-gdb", #RHBug 1967768, RHELCMP-5588
            "gcc-toolset-11-annobin", #RHBug 1968240, RHELCMP-5604
            "gcc-toolset-11-annobin-annocheck", #RHELCMP-5799
            "gcc-toolset-11-annobin-annocheck-debuginfo", #RHELCMP-5799
            "gcc-toolset-11-annobin-debuginfo", #RHELCMP-5799
            "gcc-toolset-11-annobin-plugin-gcc", #RHELCMP-5799
            "gcc-toolset-11-annobin-plugin-gcc-debuginfo", #RHELCMP-5799
            "stratisd-dracut", #RHELCMP-5609
            "eth-tools", #RHBug 1969684, RHELCMP-5632
            "sblim-gather", #RHBug 1970346, RHELCMP-5640
            "gcc-toolset-11-ltrace", #1973351, RHELCMP-5735
            "gcc-toolset-11-make", #RHBug 1973348, RHELCMP-5734
            "coreos-installer", #RHBug 1973159, RHELCMP-5730
            "ansible-collection-microsoft-sql", #RHBug 1974786, RHELCMP-5772
            "tesseract",
            "gcc-toolset-11-annobin-debuginfo", #RHELCMP-5799
            "gcc-toolset-11-binutils-devel", #RHELCMP-5799
            "gcc-toolset-11-dyninst-devel", #RHELCMP-5799
            "gcc-toolset-11-elfutils-debuginfod-client-devel", #RHELCMP-5799
            "gcc-toolset-11-elfutils-devel", #RHELCMP-5799
            "gcc-toolset-11-elfutils-libelf-devel", #RHELCMP-5799
            "gcc-toolset-11-gcc-gdb-plugin-debuginfo", #RHELCMP-5799
            "gcc-toolset-11-gcc-gdb-plugin", #RHELCMP-5799
            "gcc-toolset-11-gdb-doc", #RHELCMP-5799
            "gcc-toolset-11-gdb-gdbserver", #RHELCMP-5799
            "gcc-toolset-11-libasan-devel", #RHELCMP-5799
            "gcc-toolset-11-libatomic-devel", #RHELCMP-5799
            "gcc-toolset-11-libitm-devel", #RHELCMP-5799
            "gcc-toolset-11-liblsan-devel", #RHELCMP-5799
            "gcc-toolset-11-libstdc++-docs", #RHELCMP-5799
            "gcc-toolset-11-libtsan-devel", #RHELCMP-5799
            "gcc-toolset-11-libubsan-devel", #RHELCMP-5799
            "gcc-toolset-11-make-devel", #RHELCMP-5799
            "gcc-toolset-11-systemtap-initscript", #RHELCMP-5799
            "gcc-toolset-11-systemtap-sdt-devel", #RHELCMP-5799
            "gcc-toolset-11-systemtap-server-debuginfo", #RHELCMP-5799
            "gcc-toolset-11-systemtap-server", #RHELCMP-5799
            "gcc-toolset-11-valgrind-devel", #RHELCMP-5799
            "gcc-toolset-11-build", #RHELCMP-5799
            "eth-tools-basic", #RHBug 1969684, RHELCMP-5983
            "eth-tools-fastfabric", #RHBug 1969684, RHELCMP-5983
            "java-17-openjdk", #RHBug 1982386, RHELCMP-6030
            "samba-vfs-iouring", #RhBug 1974792
            "sevctl", #RHBug 1986032, RHELCMP-6101
            "coreos-installer-bootinfra", #RHELCMP-6277
            "gcc-toolset-11-gcc-plugin-devel", #RHELCMP-6247
        ]
    }),
    ("^BaseOS$", {
        "*": [
            "centos-stream-repos",
            "python3-debuginfo", # RCM-73713
            "python3-pyverbs", #RhBug 1856076
            "syslinux-tftpboot", #RhBug 1870276 1745205
            "samba-winexe", # Jira: RHELCMP-3276
            "accel-config", # RhBug 1917945, RHELCMP-3828
            "ima-evm-utils0", #RhBug 1925370
            "compat-hwloc1", #RhBug 1979150
        ]
    }),

    ("^BaseOS$", {
        "x86_64": [
            # Keep alsa-sof-firmware synchronized with
            # alsa-sof-firmware in the comps file
            "alsa-sof-firmware-debug", # Jira: RHELCMP-1838
        ],
    }),

    ("^BaseOS$", {
       "aarch64": [
            "opencsd", # RHELCMP-5462
        ],
    }),

    ("^Buildroot$", {
        "*": [
            "*",
        ]
    }),

    ("^PowerTools$", {
        "*": [
            "libdnf-devel",
            "librepo-devel",
            "librhsm-devel",
            "libsolv-devel",
            "libsolv-tools",
            "ibus-typing-booster-tests", # RHELCMP-4254
            "dotnet-sdk-3.1-source-built-artifacts",
            "dotnet-sdk-5.0-source-built-artifacts",
            "libcomps-devel", #RhBug 1960616
            "sblim-gather-provider", #RHBug 1959078, RHELCMP-6212
        ]
    }),

    ("^PowerTools$", {
        "aarch64": [
            "java-1.8.0-openjdk-*slowdebug*", # Jira: RHELCMP-4189
            "java-11-openjdk-*slowdebug*", # Jira: RHELCMP-4189
        ],
        "ppc64le": [
            "java-1.8.0-openjdk-*slowdebug*", # Jira: RHELCMP-4189
            "java-11-openjdk-*slowdebug*", # Jira: RHELCMP-4189
        ],
        "x86_64": [
            "java-1.8.0-openjdk-*slowdebug*", # Jira: RHELCMP-4189
            "java-11-openjdk-*slowdebug*", # Jira: RHELCMP-4189
            "dotnet5.0-build-reference-packages",
        ],
    }),
    ("^PowerTools$", {
        "x86_64": [
            "java-1.8.0-openjdk-*fastdebug*", # Jira: RHELCMP-4189
            "java-11-openjdk-*fastdebug*", # Jira: RHELCMP-4189
        ]
    }),
]
